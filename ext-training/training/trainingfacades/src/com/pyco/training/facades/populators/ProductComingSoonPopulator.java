package com.pyco.training.facades.populators;

import de.hybris.platform.commercefacades.product.converters.populator.ProductPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;

/**
 * Populates Product with isComingSoon
 * 
 * @author trang.truong
 * 
 */
public class ProductComingSoonPopulator extends	ProductPopulator {

	/**
	 * overwrite productPopulator
	 */
	@Override
	public void populate(final ProductModel source, final ProductData target)
	{
		super.populate(source, target);
		target.setCommingSoon(source.isCommningSoon());
	}
}
